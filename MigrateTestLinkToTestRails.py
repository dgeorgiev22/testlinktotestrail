#!/usr/bin/python

import getopt
import io
import os
import sys

from bs4 import BeautifulSoup as soup
from markdownify import markdownify as markdown

case_count = 0
sec_counter = 0
doc = soup(features='xml')


def read_xml(xml_file, writer_doc):
    reader_doc = soup(xml_file, 'lxml-xml')
    test_suites = reader_doc.find("testsuite", recursive=False)
    data = test_suites.find_all('testsuite', recursive=False)
    make_sections(data, writer_doc)
    return writer_doc


def make_sections(suite, root):
    tag = tagger('sections')
    for sections in suite:
        # ------------------------------
        make_section(sections, tag)
        root.append(tag)
        # -------------------------------


def make_master_section(section, sections_tag):
    # -----------------------------------
    section_counter()
    section_tag = tagger('section')
    sections_tag.append(section_tag)
    name_tag = tagger('name')
    name_tag.string = section.attrs['name']
    section_tag.append(name_tag)
    return section_tag
    # ------------------------------------


def make_section(section, sections_tag):
    # -----------------------------------
    section_counter()
    section_tag = tagger('section')
    sections_tag.append(section_tag)
    name_tag = tagger('name')
    name_tag.string = section.attrs['name']
    section_tag.append(name_tag)
    # ------------------------------------
    more_sections = section.find_all('testsuite', recursive=False)
    if more_sections is not None and len(more_sections) > 0:
        make_sections(more_sections, section_tag)

    cases = section.find_all("testcase", recursive=False)
    if cases is not None and len(cases) > 0:
        make_cases(cases, section_tag)


def make_cases(cases, section_tag):
    # ----------------------
    cases_tag = tagger('cases')
    section_tag.append(cases_tag)
    # ----------------------
    for case in cases:
        make_case(case, cases_tag)


def make_case(case, cases_tag):
    # -------------------------
    case_counter()
    case_tag = tagger('case')
    cases_tag.append(case_tag)

    title_tag = tagger('title')
    title_tag.string = case.attrs['name']
    case_tag.append(title_tag)

    template_tag = tagger('template')
    template_tag.string = 'Test Case (Steps)'
    case_tag.append(template_tag)

    priority = get_importance(get_clear_text(case, 'importance'))
    priority_tag = tagger('priority')
    priority_tag.string = priority
    case_tag.append(priority_tag)

    custom_tag = tagger('custom')
    case_tag.append(custom_tag)

    preconds_tag = tagger('preconds')
    preconds_tag.string = get_clear_text(case, 'preconditions')
    custom_tag.append(preconds_tag)

    summary_tag = tagger('summary')
    summary_tag.string = get_clear_text(case, 'summary')
    custom_tag.append(summary_tag)
    # -----------------------------------
    steps = case.find("steps")
    if steps is not None:
        make_steps(case.find("steps"), custom_tag)


def make_steps(steps, custom_tag):
    # -----------------------
    steps_tag = tagger('steps_separated')
    custom_tag.append(steps_tag)
    # -----------------------
    the_steps = steps.find_all('step')
    for step in the_steps:
        make_step(step, steps_tag)


def make_step(step, steps_tag):
    # -------------------------
    step_tag = tagger('step')
    steps_tag.append(step_tag)

    content_tag = tagger('content')
    content_tag.string = get_clear_text(step, 'actions')
    step_tag.append(content_tag)

    expected_tag = tagger('expected')
    expected_tag.string = get_clear_text(step, 'expectedresults')
    step_tag.append(expected_tag)
    # -----------------------------


'''
    -----------
    Processing and Util methods
    -----------
'''


def get_clear_text(case, tag_str):
    data = case.find(tag_str)
    precondition = markdown(data.text)  # replace with .text if . string dont work.
    precondition = sanitize_output(precondition)
    return precondition


def get_importance(str):
    if '1' in str:
        return 'Low'
    elif '2' in str:
        return 'Medium'
    elif '3' in str:
        return 'High'


def tagger(tag_name):
    global doc
    tag = doc.new_tag(tag_name)
    return tag


def case_counter():
    global case_count
    case_count += 1


def section_counter():
    global sec_counter
    sec_counter += 1


def sanitize_output(str):
    mod_str = str.strip()
    if '\n  ' in mod_str:
        mod_str = mod_str.replace('\n  ', '\n')
    if '\n ' in mod_str:
        mod_str = mod_str.replace('\n ', '\n')
    return mod_str


def file_writer(file_name, data):
    file_name = '{}.xml'.format(file_name) if '.xml' not in file_name else file_name
    print 'Creating file: {}'.format(file_name)
    with io.open(file_name, 'w', encoding='utf-8') as writer:
        writer.write(data)


def list_files(request_dir):
    listdir = os.listdir(request_dir)
    files = list()
    for item in listdir:
        if item.endswith('.xml'):
            files.append(item)
    return files


def create_if_not_exists(target_dir):
    if not os.path.exists(target_dir):
        os.makedirs(target_dir)


def process_files(file_names, resource_dir, target_dir):
    global case_count, sec_counter, doc
    if len(file_names) > 0:
        print '{} files are about to be converted'.format(len(file_names))
        print '{}'.format(file_names)
    else:
        print 'No files found in the given resource directory: "{}"'.format(resource_dir)

    for file_name in file_names:
        print '# ----------------------------------------- #'
        file_name_with_path = '{}/{}'.format(resource_dir, file_name)
        print 'About to process: {}'.format(file_name)
        with open(file_name_with_path, 'r') as f:
            read_xml(f, doc)
        create_if_not_exists(target_dir)
        target = '{}/{}'.format(target_dir, file_name)
        file_writer(target, unicode(doc))
        print 'Cases migrated: {}\nSections migrated: {}'.format(case_count, sec_counter)
        case_count = 0
        sec_counter = 0
        doc = soup(features='xml')


def main(argv):
    input_dir = ''
    output_dir = ''
    try:
        opts, args = getopt.getopt(argv, "hi:o:", ["idir=", "odir="])
    except getopt.GetoptError:
        print 'test.py -i <inputfolder> -o <outputfolder>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'MigrateTestLinkToTestRails.py -i <inputfolder> -o <outputfolder>'
            sys.exit()
        elif opt in ("-i", "--idir"):
            input_dir = arg
        elif opt in ("-o", "--odir"):
            output_dir = arg

    if len(input_dir) > 1:
        file_names = list_files(input_dir)
        output_dir = 'target' if len(output_dir) < 1 else output_dir
        process_files(file_names, input_dir, output_dir)


if __name__ == "__main__":
    main(sys.argv[1:])
