# A script to migrate Test Links xml files to Test Rails


### Requirements

In order to run this script you will need:

* Python 2.7
* pip

Once you have these two installed, you will need to run the following command to install the dependencies.

### Dependencies

Go inside the directory where the script is located, and call `pip install -r requirements.txt`

* If you have a problem installing the lxml library, simply comment it out from the requirements.txt folder and download the binary from [lxml](http://www.lfd.uci.edu/~gohlke/pythonlibs/#lxml)

### How to run

Once this is finished, you should be able to run:

`MigrateTestLinkToTestRails -i <inputfolder> -o <outputfolder>`

